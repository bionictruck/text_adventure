﻿using UnityEngine;
// Must be added whenever anything with UI is done
using UnityEngine.UI;
using System.Collections;

public class TextController : MonoBehaviour {

	// Makes a Text variable called text available to all of the methods
	// inside the TextController
	public Text text;
	// Create enumerated list called myStates containing all the states (see game design doc)
	private enum States {cell, mirror, stairs, stairs_1, stairs_2, floor, sheets_0, lock_0, cell_mirror, sheets_1, 
						 lock_1, corridor_0, corridor_1, closet_door, closet_door_1, in_closet, corridor_2,
						 corridor_3, courtyard};

	// Creates a variable used to store the current State
	private States myState;

	// Use this for initialization
	void Start () 
	// Sets the initial state to cell
	{myState = States.cell;}
	
	// Update is called once per frame
	void Update () {print (myState);
	if (myState == States.cell) {
			cell ();
		} else if (myState == States.sheets_0)  	{sheets_0 ();}
		else if (myState == States.mirror) 			{mirror ();}
		else if (myState == States.lock_0)  		{lock_0 ();}
		else if (myState == States.sheets_1) 		{sheets_1 ();}
		else if (myState == States.cell_mirror) 	{cell_mirror ();}
		else if (myState == States.lock_1) 	 		{lock_1 ();}
		else if (myState == States.corridor_0) 		{corridor_0 ();}
		else if (myState == States.stairs) 			{stairs ();}
		else if (myState == States.closet_door) 	{closet_door ();}
		else if (myState == States.floor) 			{floor ();}
		else if (myState == States.corridor_1) 		{corridor_1 ();}
		else if (myState == States.stairs_1) 		{stairs_1 ();}
		else if (myState == States.closet_door_1) 	{closet_door_1 ();}
		else if (myState == States.in_closet) 		{in_closet ();}
		else if (myState == States.corridor_2) 		{corridor_2 ();}
		else if (myState == States.corridor_3) 		{corridor_3 ();}
		else if (myState == States.stairs_2) 		{stairs_2 ();}
		else if (myState == States.courtyard) 		{courtyard ();}
	}

    void cell (){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "You are in a prison cell and you want to escape. There are some dirty sheets " +
					"on the bed, a mirror on the wall and a locked door to your cell.\n\n" +
					"Press S to view Sheets, M to view Mirror, L to view Lock";
		if (Input.GetKeyDown (KeyCode.S)) {
			myState = States.sheets_0;
		} else if (Input.GetKeyDown (KeyCode.L)) {
			myState = States.lock_0;
		} else if (Input.GetKeyDown (KeyCode.M)) {
			myState = States.mirror;
		}
	}


	void sheets_0(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "The sheets on your bed are old and dirty. You can't believe you have slept " +
					"in them. How many others have slept in these same sheets?\n\n" +
					"Press R to continue looking around your cell";
		if (Input.GetKeyDown(KeyCode.R)){
			myState = States.cell;
		}
	}

	void sheets_1(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "You look at the sheets with the mirror. Unfortunately that doesn't " +
					"improve the looks of them. In fact they look dirtier than ever.\n\n" +
					"Press R to continue looking around your cell";
		if (Input.GetKeyDown(KeyCode.R)){
			myState = States.cell_mirror;
		}
	}

	void lock_0(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "You examine the lock and can see it's one of those push button " +
					"combination locks. If only you could see the fingerprints on the buttons " +
					"that were pushed.\n\n" +
					"Press R to continue looking around your cell";
		if (Input.GetKeyDown(KeyCode.R)){
			myState = States.cell;
		}
	}

	void lock_1(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "Using the mirror you can see which buttons have been pressed. " +
					"Through some trial and error you finally have the combination.\n\n" +
					"Press U to unlock the cell door or R to keep looking around your cell.";
		if (Input.GetKeyDown(KeyCode.U)){
			myState = States.corridor_0;
		} else if (Input.GetKeyDown(KeyCode.R)){
			myState = States.cell_mirror;
		}
	}


		void cell_mirror (){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "You are still in your cell but now you're holding the mirror. " +
					"You could try using the mirror while you look around your cell.\n\n" +
					"Press S to view Sheets, L to view Lock";
		if (Input.GetKeyDown(KeyCode.S)) {
			myState = States.sheets_1;
		} else if (Input.GetKeyDown(KeyCode.L)) {
			myState = States.lock_1;
		}
	}

	void mirror(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "Examining the mirror you can see that it is loosely held to the wall. " +
					"It shouldn't take much effort to pry it loose.\n\n" +
					"Press T to take the mirror or R to continue looking around your cell";
		if (Input.GetKeyDown(KeyCode.R)){
			myState = States.cell;
		} else if (Input.GetKeyDown(KeyCode.T)){
			myState = States.cell_mirror;
		}
	}

	void corridor_0(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "The door swings open and leads down some stairs to a corridor. " +
					"There is a door at the end of the corridor. There is also quite " +
					"a bit of garbage lying on the floor.\n\n" +
					"Press S to look up the stairs, D to look at the door or F to look " +
					"at the garbage on the floor.";
		if (Input.GetKeyDown(KeyCode.S)){
			myState = States.stairs;
		} else if (Input.GetKeyDown(KeyCode.D)){
			myState = States.closet_door;
		} else if (Input.GetKeyDown(KeyCode.F)){
			myState = States.floor;
		}
	}
	
	void stairs(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "The stairs lead back up to your cell. " +
					"You don't actually want to go back do you?\n\n" +
					"Press R to return to the corridor.";
		if (Input.GetKeyDown(KeyCode.R)){
			myState = States.corridor_0;
		}
	}
	
	void closet_door(){
			// Wrapped text using + to concatenate and \n to add a space for new line. 
			// \n\n adds a double space
			text.text = "You run to the door and turn the knob only to find it locked. " +
						"Perhaps something in this garbage on the floor could unlock it\n\n" +
						"Press R to return to the corridor.";
			if (Input.GetKeyDown(KeyCode.R)){
				myState = States.corridor_0;
			}
		}
		
	void floor(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "You dig through the garbage on the floor and find mostly papers. " +
					"Out of the corner of your eye you spot a hairclip. " +
					"Something is better than nothing you suppose.\n\n" +
					"Press R to return to the corridor, H to pick up the hairclip.";
		if (Input.GetKeyDown(KeyCode.R)){
			myState = States.corridor_0;
		} else if (Input.GetKeyDown(KeyCode.H)){
			myState = States.corridor_1;
		}
	}
	
	void corridor_1(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "Now holding the hairclip you take another look at your surroundings. " +
					"This hairclip must be useful for more than looking pretty.\n\n" +
					"Press S to look up the stairs, D to look at the door.";
		if (Input.GetKeyDown(KeyCode.S)){
			myState = States.stairs_1;
		} else if (Input.GetKeyDown(KeyCode.D)){
			myState = States.closet_door_1;
		}
	}
	
	void stairs_1(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "Well...you'd be back in your cell with a hairclip. So you got that " +
					"going for you. Which is nice.\n\n" +
					"Press R to return to the corridor.";
		if (Input.GetKeyDown(KeyCode.R)){
			myState = States.corridor_1;
		}
	}
	
	void closet_door_1(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "Now holding the hairclip you take another look at your surroundings. " +
					"The door is still there....locked.\n\n" +
					"Press P to pick the lock, R to return to the corridor.";
		if (Input.GetKeyDown(KeyCode.R)){
			myState = States.corridor_1;
		} else if (Input.GetKeyDown(KeyCode.P)){
			myState = States.in_closet;
		}
	}

	void in_closet(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "You've used the hairclip to pick the lock and open the closet door. " +
					"An outfit for the cleaning crew is hanging along with several " +
					"empty boxes.\n\n" +
					"Press D to dress with the cleaners outfit, R to return to the corridor.";
		if (Input.GetKeyDown(KeyCode.R)){
			myState = States.corridor_2;
		} else if (Input.GetKeyDown(KeyCode.D)){
			myState = States.corridor_3;
		}
	}

		void corridor_2(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "Leaving the closet you can see an exit up another set of stairs " + 
					"to the courtyard. You dare not venture out into the courtyard wearing your " +
					"prison clothes.\n\n" +
					"Press B to go back to the closet, S to go back up the stairs to your cell.";
		if (Input.GetKeyDown(KeyCode.B)){
			myState = States.in_closet;
		} else if (Input.GetKeyDown(KeyCode.S)){
			myState = States.stairs_2;
		}
	}

		void corridor_3(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "Inside the closet you change out of your prison clothes " +
					"into the cleaners outfit. Back in the corridor you see the stairs " +
					"leading out to the courtyard. Now is your chance to escpae!\n\n" +
					"Press C to go out into the courtyard. " +
					"U to change back into your prison clothes.";
		if (Input.GetKeyDown(KeyCode.C)){
			myState = States.courtyard;
		} else if (Input.GetKeyDown(KeyCode.U)){
			myState = States.in_closet;
		}
	}

		void stairs_2(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "The closet is open and here you are back in your cell. " +
					"Your situation has not improved.\n\n" +
					"Press R to return to the corridor.";
		if (Input.GetKeyDown(KeyCode.R)){
			myState = States.corridor_2;
		}
	}

		void courtyard(){
		// Wrapped text using + to concatenate and \n to add a space for new line. 
		// \n\n adds a double space
		text.text = "You walk into the courtyard. No one notices you in your " +
					"borrowed cleaners outfit. The sun beats down on your face " +
					"as you make your way out of the prison to freedom.\n\n" +
					"Press P to play again.";
		if (Input.GetKeyDown(KeyCode.P)){
			myState = States.cell;
		}
	}

}
